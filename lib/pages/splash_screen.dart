import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ethical_capital_app/utils/ethical.dart';
import 'package:ethical_capital_app/utils/my_navigator.dart';
import 'package:flutter_svg/flutter_svg.dart';

final String splashlogo = 'lib/assets/images/splashLogo.svg';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 5), () => MyNavigator.goToBenefits(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.pink[800]),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      SizedBox.fromSize(
                        child: SvgPicture.asset(
                          splashlogo,
                          semanticsLabel: 'Splash logo',
                          color: Colors.white,
                          height: 63.0,
                          width: 57.0,
                          // allowDrawingOutsideViewBox: true,
                        ),
                        size: Size(63.0, 57.0),
                      ), 
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Text(
                        Ethical.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}