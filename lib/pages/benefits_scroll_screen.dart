import 'package:ethical_capital_app/utils/ethical.dart';
import 'package:flutter/material.dart';
import 'package:ethical_capital_app/utils/my_navigator.dart';
import 'package:ethical_capital_app/widgets/walkthrough.dart';
import 'package:flutter_svg/flutter_svg.dart';

final String benefitsIcon1 = 'lib/assets/images/benefitsIcon1.svg';
final String benefitsIcon2 = 'lib/assets/images/benefitsIcon2.svg';
final String benefitsIcon3 = 'lib/assets/images/benefitsIcon3.svg';
final String benefitsIcon4 = 'lib/assets/images/benefitsIcon4.svg';

class IntroScreen extends StatefulWidget {
  @override
  IntroScreenState createState() {
    return IntroScreenState();
  }
}

class IntroScreenState extends State<IntroScreen> {
  final PageController controller = new PageController();
  int currentPage = 0;
  bool lastPage = false;

  void _onPageChanged(int page) {
    setState(() {
      currentPage = page;
      if (currentPage == 3) {
        lastPage = true;
      } else {
        lastPage = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFEEEEEE),
      padding: EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 3,
            child: PageView(
              children: <Widget>[
                Walkthrough(
                  title: Ethical.wt1,
                  content: Ethical.wc1,
                  imageSVGIcon: benefitsIcon1,
                ),
                Walkthrough(
                  title: Ethical.wt2,
                  content: Ethical.wc2,
                  imageSVGIcon: benefitsIcon2,
                ),
                Walkthrough(
                  title: Ethical.wt3,
                  content: Ethical.wc3,
                  imageSVGIcon: benefitsIcon3,
                ),
                Walkthrough(
                  title: Ethical.wt4,
                  content: Ethical.wc4,
                  imageSVGIcon: benefitsIcon4,
                ),
              ],
              controller: controller,
              onPageChanged: _onPageChanged,
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text(lastPage ? "" : Ethical.skip,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0)),
                  onPressed: () =>
                      lastPage ? null : MyNavigator.goToStart(context),
                ),
                FlatButton(
                  child: Text(lastPage ? Ethical.gotIt : Ethical.next,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0)),
                  onPressed: () => lastPage
                      ? MyNavigator.goToStart(context)
                      : controller.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.easeIn),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
