import 'package:flutter/material.dart';
import 'package:ethical_capital_app/utils/ethical.dart';
import 'package:flutter_svg/flutter_svg.dart';

final String startScreenImage1 = 'lib/assets/images/startScreenImage1.svg';
final String startScreenImage2 = 'lib/assets/images/startScreenImage2.svg';
final String startScreenImage3 = 'lib/assets/images/startScreenImage3.svg';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.white),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Padding(
                        padding: EdgeInsets.only(top: 150.0),
                      ),

                      SizedBox.fromSize(
                        child: SvgPicture.asset(
                          startScreenImage1,
                          semanticsLabel: 'StartScreenImage1',
                          height: 130.0,
                          width: 98.0,
                          // allowDrawingOutsideViewBox: true,
                        ),
                        size: Size(130.0, 98.0),
                      ), 
                      
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),

                      Icon(
                        Icons.add,
                        color: Colors.pink[800],
                        size: 32.0,
                        semanticLabel: 'Text to announce in accessibility modes',
                      ),

                      SizedBox.fromSize(
                        child: SvgPicture.asset(
                          startScreenImage2,
                          semanticsLabel: 'StartScreenImage2',
                          height: 130.0,
                          width: 98.0,
                          // allowDrawingOutsideViewBox: true,
                        ),
                        size: Size(130.0, 98.0),
                      ), 
                      
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.pink[800],
                        size: 32.0,
                        semanticLabel: 'Text to announce in accessibility modes',
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      SizedBox.fromSize(
                        child: SvgPicture.asset(
                          startScreenImage3,
                          semanticsLabel: 'StartScreenImage3',
                          height: 130.0,
                          width: 98.0,
                          // allowDrawingOutsideViewBox: true,
                        ),
                        size: Size(130.0, 98.0),
                      ),

                      Expanded(
                        child: Align(
                          alignment: FractionalOffset.bottomCenter,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: 55.0,
                            child: RaisedButton(
                              color: Colors.pink[800],
                              textColor: Colors.white,
                              disabledColor: Colors.grey,
                              disabledTextColor: Colors.black ,
                              padding: EdgeInsets.all(8.0),
                              splashColor: Colors.pinkAccent,
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(4.0),
                              ),
                              
                              onPressed: () {
                                /*...*/
                              },
                              child: Text(
                                "Start",
                                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                      ),
                      
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}