import 'package:flutter/material.dart';
import 'package:ethical_capital_app/pages/start_screen.dart';
import 'package:ethical_capital_app/pages/benefits_scroll_screen.dart';
import 'package:ethical_capital_app/pages/splash_screen.dart';

var routes = <String, WidgetBuilder>{
  "/benefits_scroll_screen": (BuildContext context) => IntroScreen(),
  "/start_screen": (BuildContext context) => StartScreen(),
};

void main() => runApp(new MaterialApp(
    theme:
        ThemeData(primaryColor: Colors.red, accentColor: Colors.yellowAccent),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes));
