import 'package:flutter/material.dart';

class MyNavigator {
  static void goToBenefits(BuildContext context) {
    Navigator.pushNamed(context, "/benefits_scroll_screen");
  }

  static void goToStart(BuildContext context) {
    Navigator.pushNamed(context, "/start_screen");
  }
}
