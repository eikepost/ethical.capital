class Ethical {
  static const String name = "Ethical Capital";
  static const String store = "Ethical Capital App";
  static const String wt1 = "Social Impact";
  static const String wc1 =
      "All the investment opportunities on\n Ethical Capital are screened for social\n impact.";
  static const String wt2 = "Invest starting at 1 USD";
  static const String wc2 =
      "You can do any investment starting at\n only 1 USD";
  static const String wt3 = "Free";
  static const String wc3 =
      "Ethical Capital does not charge any\n trading fees";
  static const String wt4 = "UN Sustainable Development Goals";
  static const String wc4 =
      "The app is based on the UN \n framework of the UN sustainable\n development goals";
  static const String skip = "SKIP";
  static const String next = "NEXT";
  static const String gotIt = "GOT IT";
}